const express = require("express");

const AuthenticationRoutes = require("./routes/authentication.routes");

const app = express();

app.use("/otp", AuthenticationRoutes);

module.exports = app;